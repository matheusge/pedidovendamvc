﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.Interfaces.Repositories;
using ProjetoPedidoVenda.Domain.Interfaces.Services;

namespace ProjetoPedidoVenda.Domain.Services
{
    public class ClienteService : ServiceBase<Cliente>, IClienteService
    {
        private readonly IClienteRepository _clienteRepository;

        public ClienteService(IClienteRepository clienteRepository)
            :base(clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }
    }
}
