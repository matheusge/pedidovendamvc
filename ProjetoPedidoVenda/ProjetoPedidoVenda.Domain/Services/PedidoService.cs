﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.Interfaces.Repositories;
using ProjetoPedidoVenda.Domain.Interfaces.Services;

namespace ProjetoPedidoVenda.Domain.Services
{
    public class PedidoService : ServiceBase<Pedido>, IPedidoService
    {
        private readonly IPedidoRepository _pedidoRepository;

        public PedidoService(IPedidoRepository pedidoRepository)
            :base(pedidoRepository)
        {
            _pedidoRepository = pedidoRepository;
        }
    }
}
