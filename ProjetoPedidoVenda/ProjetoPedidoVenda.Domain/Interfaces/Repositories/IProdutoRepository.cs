﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Domain.Interfaces.Repositories
{
    public interface IProdutoRepository : IRepositoryBase<Produto>
    {
    }
}
