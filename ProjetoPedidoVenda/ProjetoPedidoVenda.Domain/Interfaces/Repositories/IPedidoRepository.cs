﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Domain.Interfaces.Repositories
{
    public interface IPedidoRepository : IRepositoryBase<Pedido>
    {
    }
}
