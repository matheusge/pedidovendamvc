﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Domain.Interfaces.Services
{
    public interface IPedidoItemService : IServiceBase<PedidoItem>
    {
    }
}
