﻿using System.Collections.Generic;

namespace ProjetoPedidoVenda.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        ICollection<TEntity> GetAll();
        TEntity GetById(int id);
        void Add(TEntity obj);
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
