﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Domain.Interfaces.Services
{
    public interface IPedidoService : IServiceBase<Pedido>
    {
    }
}
