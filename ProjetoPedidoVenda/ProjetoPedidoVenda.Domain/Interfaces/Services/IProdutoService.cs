﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Domain.Interfaces.Services
{
    public interface IProdutoService : IServiceBase<Produto>
    {
    }
}
