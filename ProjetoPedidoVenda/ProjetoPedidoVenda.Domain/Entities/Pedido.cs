﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoPedidoVenda.Domain.Entities
{
    public class Pedido
    {
        public Pedido(){}

        public Pedido(DateTime dataEntrega)
        {
            if (dataEntrega.Date < DateTime.Now.Date)
            {
                throw new ArgumentException("Data de entrega deve ser maior ou igual a data atual");
            }

            this.DataEntrega = dataEntrega;
            this.PedidoItem = new List<PedidoItem>();
        }

        public int Id { get; set; }
        public DateTime DataEntrega { get; set; }

        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }

        public virtual ICollection<PedidoItem> PedidoItem { get; set; }
        //public void AdicionarProduto(PedidoItem item)
        //{
        //    if (PedidoItem.Any())
        //    {
        //        var p = PedidoItem.Where(x => x.Produto.Id == item.Produto.Id);
        //        if(p.Any())
        //        {
        //            throw new ArgumentException("Este produto já existe no seu pedido");
        //        }
        //    }

        //    PedidoItem.Add(item);
        //}

        public decimal Valortotal
        {
            get
            {
                if (PedidoItem.Any())
                {
                    return PedidoItem.Sum(x => x.ValorTotal);
                }
                return 0;
            }
        }
    }
}
