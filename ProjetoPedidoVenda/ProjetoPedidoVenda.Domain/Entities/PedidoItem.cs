﻿using System;

namespace ProjetoPedidoVenda.Domain.Entities
{
    public class PedidoItem
    {
        public PedidoItem(){}
        public PedidoItem(Produto p, decimal q)
        {
            if(q <= 0){ throw new ArgumentException("A quantidade do produto deve ser maior que zero"); }
            if(p.Valor <= 0) { throw new ArgumentException("O valor do produto deve ser maior que zero"); }

            //this.Produto = p;
            this.Quantidade = q;
            this.Valor = p.Valor;
            this.ProdutoId = p.Id;
        }

        public int Id { get; set; }

        public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }

        public decimal Quantidade { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorTotal
        {
            get { return Quantidade * Valor; }
        }

        public int PedidoId { get; set; } 
        public virtual Pedido Pedido { get; set; }
    }
}
