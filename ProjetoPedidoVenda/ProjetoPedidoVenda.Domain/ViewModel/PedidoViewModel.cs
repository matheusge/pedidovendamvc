﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoPedidoVenda.Domain.ViewModel
{
    public class PedidoViewModel
    {
        public int Id { get; set; }
        public DateTime DataEntrega { get; set; }

        public int ClienteId { get; set; }
        public ClienteViewModel Cliente { get; set; }

        public ICollection<PedidoItemViewModel> PedidoItem { get; set; }

        public decimal ValorTotal
        {
            get
            {
                if (PedidoItem.Any())
                {
                    return PedidoItem.Sum(x => x.ValorTotal);
                }
                return 0;
            }
        }
    }
}
