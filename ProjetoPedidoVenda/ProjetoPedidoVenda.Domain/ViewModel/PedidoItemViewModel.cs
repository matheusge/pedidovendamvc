﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoPedidoVenda.Domain.ViewModel
{
    public class PedidoItemViewModel
    {
        public int Id { get; set; }

        public int ProdutoId { get; set; }
        public virtual ProdutoViewModel Produto { get; set; }

        public decimal Quantidade { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorTotal
        {
            get { return Quantidade * Valor; }
        }

        public int PedidoId { get; set; }
        public virtual PedidoViewModel Pedido { get; set; }
    }
}
