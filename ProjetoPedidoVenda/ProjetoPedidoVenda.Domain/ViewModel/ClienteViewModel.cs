﻿namespace ProjetoPedidoVenda.Domain.ViewModel
{
    public class ClienteViewModel
    {
        public int Id { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }
    }
}
