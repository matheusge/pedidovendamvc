﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoPedidoVenda.Infra.Data.Repositories;
using ProjetoPedidoVenda.Domain.Entities;
using System.Linq;
using System;

namespace ProjetoPedidoVenda.UnitTest.Repository
{
    /// <summary>
    /// Summary description for UnitTestPedidoRepository
    /// </summary>
    [TestClass]
    public class UnitTestPedidoRepository
    {
        public UnitTestPedidoRepository()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AdicionarUmPedidoComUmItem()
        {
            PedidoRepository pedidoRepository = new PedidoRepository();
            ProdutoRepository produtoRepository = new ProdutoRepository();
            ClienteRepository clienteRepository = new ClienteRepository();

            Pedido p;
            PedidoItem pI;
            Cliente c;
            DateTime dataEntrega = DateTime.Now;

            var produtos = produtoRepository.GetAll();
            if (!produtos.Any()){
                Assert.Fail("Necessário possuir um produto cadastrado!");
            }

            var clientes = clienteRepository.GetAll();
            if (!clientes.Any())
            {
                c = new Cliente() { CPF = "01234567891", Nome = "Cliente Genérico" };
                clienteRepository.Add(c);
            }
            else
            {
                c = clientes.FirstOrDefault();
            }

            var produto = produtos.FirstOrDefault();
            pI = new PedidoItem(produto, 1);
            pI.ProdutoId = produto.Id;
            p = new Pedido(dataEntrega);
            p.PedidoItem.Add(pI);
            //p.Cliente = c;
            p.ClienteId = c.Id;

            pedidoRepository.Add(p);
        }

        [TestMethod]
        public void BuscarPrimeiroPedido()
        {
            PedidoRepository pedidoRepository = new PedidoRepository();
            var pedidos = pedidoRepository.GetAll();
            if (!pedidos.Any())
            {
                Assert.Fail("Necessário cadastrar um pedido antes de executar o teste");
            }

            Pedido p = pedidos.FirstOrDefault();

            Pedido pFind = pedidoRepository.GetById(p.Id);


        }
    }
}
