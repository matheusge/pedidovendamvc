﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.UnitTest.Domain
{
    /// <summary>
    /// Summary description for UnitTestPedido
    /// </summary>
    [TestClass]
    public class UnitTestPedido
    {
        public UnitTestPedido()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void NovoPedidoComDataEntregaAtual()
        {
            Pedido p = new Pedido(DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NovoPedidoComDataEntregaPassada()
        {
            Pedido p = new Pedido(DateTime.Now.AddDays(-1));
        }

        [TestMethod]
        public void NovoPedidoAdicionandoNovoPedidoItem()
        {
            Produto prd = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 10 };
            PedidoItem pI = new PedidoItem(prd, 10);
            Pedido p = new Pedido(DateTime.Now);
            p.PedidoItem.Add(pI);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NovoPedidoAdicionandoPedidoItemComProdutoRepetido()
        {
            Produto prd = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 10 };
            PedidoItem pI = new PedidoItem(prd, 10);
            Pedido p = new Pedido(DateTime.Now);
            //p.AdicionarProduto(pI);
            p.PedidoItem.Add(pI);
            p.PedidoItem.Add(pI);
        }

        [TestMethod]
        public void NovoPedidoComMaisDeUmPedidoItem()
        {
            Produto prd = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 10 };
            Produto prd2 = new Produto() { Id = 2, Descricao = "Produto 2", Valor = 10 };
            PedidoItem pI = new PedidoItem(prd, 10);
            PedidoItem pI2 = new PedidoItem(prd2, 10);
            Pedido p = new Pedido(DateTime.Now);
            p.PedidoItem.Add(pI);
            p.PedidoItem.Add(pI2);
        }
    }
}
