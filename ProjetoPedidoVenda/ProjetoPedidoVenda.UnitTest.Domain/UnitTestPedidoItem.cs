﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.UnitTest.Domain
{
    /// <summary>
    /// Summary description for UnitTestPedidoItem
    /// </summary>
    [TestClass]
    public class UnitTestPedidoItem
    {
        public UnitTestPedidoItem()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NovoPedidoItemComQuantidadeIgualZero()
        {
            Produto p = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 10 };
            PedidoItem pI = new PedidoItem(p, 0);
        }

        [TestMethod]
        public void NovoPedidoItemComQuantidadeMaiorZero()
        {
            Produto p = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 10 };
            PedidoItem pI = new PedidoItem(p, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NovoPedidoItemComValorIgualZero()
        {
            Produto p = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 0 };
            PedidoItem pI = new PedidoItem(p, 1);
        }

        [TestMethod]
        public void NovoPedidoItemComValorMaiorZero()
        {
            Produto p = new Produto() { Id = 1, Descricao = "Produto 1", Valor = 10 };
            PedidoItem pI = new PedidoItem(p, 1);
        }
    }
}
