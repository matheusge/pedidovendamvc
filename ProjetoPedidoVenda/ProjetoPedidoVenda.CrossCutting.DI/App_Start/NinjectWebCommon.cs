//[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ProjetoPedidoVenda.CrossCutting.DI.App_Start.NinjectWebCommon), "Start")]
//[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ProjetoPedidoVenda.CrossCutting.DI.App_Start.NinjectWebCommon), "Stop")]

namespace ProjetoPedidoVenda.CrossCutting.DI.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.WebApi.DependencyResolver;
    using ProjetoPedidoVenda.Application;
    using ProjetoPedidoVenda.Application.Interface;
    using ProjetoPedidoVenda.Domain.Interfaces.Services;
    using ProjetoPedidoVenda.Domain.Services;
    using ProjetoPedidoVenda.Domain.Interfaces.Repositories;
    using ProjetoPedidoVenda.Infra.Data.Repositories;
    using ProjetoPedidoVenda.CrossCutting.Mapping.Mappers;
    using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
    

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            kernel.Bind<IClienteAppService>().To<ClienteAppService>();
            kernel.Bind<IProdutoAppService>().To<ProdutoAppService>();
            kernel.Bind<IPedidoAppService>().To<PedidoAppService>();

            kernel.Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            kernel.Bind<IClienteService>().To<ClienteService>();
            kernel.Bind<IProdutoService>().To<ProdutoService>();
            kernel.Bind<IPedidoService>().To<PedidoService>();

            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IClienteRepository>().To<ClienteRepository>();
            kernel.Bind<IProdutoRepository>().To<ProdutoRepository>();
            kernel.Bind<IPedidoRepository>().To<PedidoRepository>();

            kernel.Bind(typeof(IMapperBase<,>)).To(typeof(MapperBase<,>));
            kernel.Bind<IClienteMapper>().To<ClienteMapper>();
            kernel.Bind<IProdutoMapper>().To<ProdutoMapper>();
            kernel.Bind<IPedidoMapper>().To<PedidoMapper>();
            kernel.Bind<IPedidoItemMapper>().To<PedidoItemMapper>();
        }        
    }
}
