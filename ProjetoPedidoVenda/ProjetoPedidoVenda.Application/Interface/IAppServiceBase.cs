﻿using System.Collections.Generic;

namespace ProjetoPedidoVenda.Application.Interface
{
    public interface IAppServiceBase<TEntity> where TEntity : class
    {
        TEntity GetById(int id);
        ICollection<TEntity> GetAll();
        void Add(TEntity obj);
        void Update(TEntity obj);
        void Remove(TEntity obj);
        void Dispose();
    }
}
