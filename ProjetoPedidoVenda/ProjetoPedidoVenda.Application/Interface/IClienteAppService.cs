﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Application.Interface
{
    public interface IClienteAppService : IAppServiceBase<Cliente>
    {
    }
}
