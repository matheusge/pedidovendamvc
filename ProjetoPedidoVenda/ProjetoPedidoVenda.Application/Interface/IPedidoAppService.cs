﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Application.Interface
{
    public interface IPedidoAppService : IAppServiceBase<Pedido>
    {
    }
}
