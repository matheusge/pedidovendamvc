﻿using ProjetoPedidoVenda.Domain.Entities;

namespace ProjetoPedidoVenda.Application.Interface
{
    public interface IProdutoAppService : IAppServiceBase<Produto>
    {
    }
}
