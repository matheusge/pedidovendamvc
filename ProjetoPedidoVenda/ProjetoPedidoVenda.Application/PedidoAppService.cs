﻿using ProjetoPedidoVenda.Application.Interface;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.Interfaces.Services;

namespace ProjetoPedidoVenda.Application
{
    public class PedidoAppService : AppServiceBase<Pedido>, IPedidoAppService
    {
        private readonly IPedidoService _pedidoService;

        public PedidoAppService(IPedidoService pedidoService)
            :base(pedidoService)
        {
            _pedidoService = pedidoService;
        }
    }
}
