﻿using ProjetoPedidoVenda.Application.Interface;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.Interfaces.Services;

namespace ProjetoPedidoVenda.Application
{
    public class ClienteAppService : AppServiceBase<Cliente>, IClienteAppService
    {
        private readonly IClienteService _clienteService;

        public ClienteAppService(IClienteService clienteService)
            :base(clienteService)
        {
            _clienteService = clienteService;
        }
    }
}
