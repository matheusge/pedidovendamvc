﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ProjetoPedidoVenda.CrossCutting.DI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ProjetoPedidoVenda.CrossCutting.DI.App_Start.NinjectWebCommon), "Stop")]
namespace ProjetoPedidoVenda.Presentation.WebAPI.App_Start
{
    public static class InitializeNinjectWebCommon
    {
    }
}