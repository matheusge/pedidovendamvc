﻿using ProjetoPedidoVenda.Application.Interface;
using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
using ProjetoPedidoVenda.Domain.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ProjetoPedidoVenda.Presentation.WebAPI.Controllers
{
    public class PedidoController : ApiController
    {
        private IPedidoAppService _pedidoAppService;

        private IPedidoMapper _pedidoMapper;
        private IPedidoItemMapper _pedidoItemMapper;
        private IProdutoMapper _produtoMapper;
        private IClienteMapper _clienteMapper;

        public PedidoController(IPedidoAppService pedidoAppService, 
            IPedidoMapper pedidoMapper, 
            IPedidoItemMapper pedidoItemMapper,
            IProdutoMapper produtoMapper,
            IClienteMapper clienteMapper)
        {
            _pedidoAppService = pedidoAppService;
            _pedidoMapper = pedidoMapper;
            _pedidoItemMapper = pedidoItemMapper;
            _produtoMapper = produtoMapper;
            _clienteMapper = clienteMapper;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var pedidosDomain = _pedidoAppService.GetAll();

                IList<PedidoViewModel> pedidosViewModel = new List<PedidoViewModel>();
                if (pedidosDomain.Any())
                {
                    foreach(var p in pedidosDomain)
                    {
                        PedidoViewModel pVM = new PedidoViewModel()
                        {
                            Id = p.Id,
                            Cliente = _clienteMapper.MapDomainToViewModel(p.Cliente),
                            ClienteId = p.ClienteId,
                            DataEntrega = p.DataEntrega,
                        };

                        pVM.PedidoItem = new List<PedidoItemViewModel>();

                        if (p.PedidoItem.Any())
                        {
                            foreach (var pI in p.PedidoItem)
                            {
                                PedidoItemViewModel pIVM = new PedidoItemViewModel()
                                {
                                    Id = pI.Id,
                                    Quantidade = pI.Quantidade,
                                    Valor = pI.Valor,
                                    ProdutoId = pI.ProdutoId,
                                    Produto = _produtoMapper.MapDomainToViewModel(pI.Produto)
                                };

                                pVM.PedidoItem.Add(pIVM);
                            }
                        }
                        pedidosViewModel.Add(pVM);
                    }
                }

                return Json(pedidosViewModel);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }

    }
}
