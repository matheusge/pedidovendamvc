﻿using ProjetoPedidoVenda.Application.Interface;
using ProjetoPedidoVenda.CrossCutting.Mapping;
using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Text;

namespace ProjetoPedidoVenda.Presentation.WebAPI.Controllers
{
    public class ClienteController : ApiController
    {
        private IClienteAppService _clienteAppService;
        private IClienteMapper _clienteMapper;

        public ClienteController(IClienteAppService clienteAppService, IClienteMapper clienteMapper)
        {
            _clienteAppService = clienteAppService;
            _clienteMapper = clienteMapper;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var clientesDomain = _clienteAppService.GetAll();
                var clientesViewModel = _clienteMapper.MapDomainToViewModel(clientesDomain);
                return Json(clientesViewModel);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var clienteDomain = _clienteAppService.GetById(id);
                var clienteViewModel = _clienteMapper.MapDomainToViewModel(clienteDomain);
                return Json(clienteViewModel);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }
    }
}
