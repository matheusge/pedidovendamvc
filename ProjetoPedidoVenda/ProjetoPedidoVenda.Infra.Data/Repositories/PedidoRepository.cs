﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.Interfaces.Repositories;

namespace ProjetoPedidoVenda.Infra.Data.Repositories
{
    public class PedidoRepository : RepositoryBase<Pedido>, IPedidoRepository
    {
    }
}
