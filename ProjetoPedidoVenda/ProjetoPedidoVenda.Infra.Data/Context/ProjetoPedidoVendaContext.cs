﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Infra.Data.EntityConfig;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ProjetoPedidoVenda.Infra.Data.Context
{
    public class ProjetoPedidoVendaContext : DbContext
    {
        public ProjetoPedidoVendaContext()
            : base("ProjetoPedidoVendaContext")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
        public DbSet<PedidoItem> PedidoItem { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Configurations.Add(new ClienteConfiguration());
            modelBuilder.Configurations.Add(new ProdutoConfiguration());
            modelBuilder.Configurations.Add(new PedidoConfiguration());
            modelBuilder.Configurations.Add(new PedidoItemConfiguration());

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

        }
    }
}
