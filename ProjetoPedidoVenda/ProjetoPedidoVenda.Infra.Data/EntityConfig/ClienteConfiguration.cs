﻿using ProjetoPedidoVenda.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ProjetoPedidoVenda.Infra.Data.EntityConfig
{
    public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(100);

            Property(x => x.CPF)
                .IsRequired()
                .HasMaxLength(11);
        }
    }
}
