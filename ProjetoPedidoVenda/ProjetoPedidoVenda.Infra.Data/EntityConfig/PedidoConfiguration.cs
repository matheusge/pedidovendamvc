﻿using ProjetoPedidoVenda.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ProjetoPedidoVenda.Infra.Data.EntityConfig
{
    public class PedidoConfiguration : EntityTypeConfiguration<Pedido>
    {
        public PedidoConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.DataEntrega);

            //HasRequired(x => x.PedidoItem)
            //    .WithMany();

            HasRequired(x => x.Cliente)
                .WithMany()
                .HasForeignKey(x => x.ClienteId);
        }
    }
}
