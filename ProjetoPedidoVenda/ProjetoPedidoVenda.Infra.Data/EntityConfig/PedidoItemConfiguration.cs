﻿using ProjetoPedidoVenda.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ProjetoPedidoVenda.Infra.Data.EntityConfig
{
    public class PedidoItemConfiguration : EntityTypeConfiguration<PedidoItem>
    {
        public PedidoItemConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Valor);
            Property(x => x.Quantidade);

            HasRequired(x => x.Produto)
                .WithMany()
                .HasForeignKey(x => x.ProdutoId);

            HasRequired(x => x.Pedido)
                .WithMany()
                .HasForeignKey(x => x.PedidoId);
        }
    }
}
