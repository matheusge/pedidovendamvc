﻿using ProjetoPedidoVenda.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ProjetoPedidoVenda.Infra.Data.EntityConfig
{
    public class ProdutoConfiguration : EntityTypeConfiguration<Produto>
    {
        public ProdutoConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Descricao)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Valor)
                .IsRequired();
        }
    }
}
