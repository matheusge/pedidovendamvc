﻿using System.Collections;
using System.Collections.Generic;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Interface
{
    public interface IMapperBase<TEntity, TEntityViewModel>
        where TEntity : class
        where TEntityViewModel : class
    {
        TEntity MapViewModelToDomain(TEntityViewModel p);
        IEnumerable<TEntity> MapViewModelToDomain(IEnumerable<TEntityViewModel> p);

        TEntityViewModel MapDomainToViewModel(TEntity p);
        IEnumerable<TEntityViewModel> MapDomainToViewModel(IEnumerable<TEntity> p);
    }
}
