﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Interface
{
    public interface IClienteMapper : IMapperBase<Cliente, ClienteViewModel>
    {
    }
}
