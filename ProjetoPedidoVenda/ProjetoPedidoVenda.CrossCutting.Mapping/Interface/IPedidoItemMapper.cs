﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Interface
{
    public interface IPedidoItemMapper : IMapperBase<PedidoItem, PedidoItemViewModel>
    {
    }
}
