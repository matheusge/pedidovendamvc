﻿using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Interface
{
    public interface IPedidoMapper : IMapperBase<Pedido, PedidoViewModel>
    {
    }
}
