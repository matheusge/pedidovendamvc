﻿using AutoMapper;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<Cliente, ClienteViewModel>();
            CreateMap<Produto, ProdutoViewModel>();

            CreateMap<Pedido, PedidoViewModel>()
                .ForMember(x => x.PedidoItem, z => z.Ignore())
                .ForMember(x => x.Cliente, z => z.Ignore());

            CreateMap<PedidoItem, PedidoItemViewModel>()
                .ForMember(x => x.Pedido, z => z.Ignore())
                .ForMember(x => x.Produto, z => z.Ignore());
        }
    }
}
