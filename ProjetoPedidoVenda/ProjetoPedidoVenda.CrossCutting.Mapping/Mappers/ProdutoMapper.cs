﻿using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Mappers
{
    public class ProdutoMapper : MapperBase<Produto, ProdutoViewModel>, IProdutoMapper
    {
    }
}
