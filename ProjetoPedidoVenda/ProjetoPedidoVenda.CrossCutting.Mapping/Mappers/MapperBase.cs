﻿using System;
using System.Collections.Generic;
using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
using AutoMapper;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Mappers
{
    public class MapperBase<TEntity, TEntityViewModel> : IMapperBase<TEntity, TEntityViewModel>
        where TEntity : class
        where TEntityViewModel : class
    {
        public TEntityViewModel MapDomainToViewModel(TEntity p)
        {
            return Mapper.Map<TEntityViewModel>(p);
        }

        public IEnumerable<TEntityViewModel> MapDomainToViewModel(IEnumerable<TEntity> p)
        {
            return Mapper.Map<IEnumerable<TEntityViewModel>>(p);
        }

        public TEntity MapViewModelToDomain(TEntityViewModel p)
        {
            return Mapper.Map<TEntity>(p);
        }

        public IEnumerable<TEntity> MapViewModelToDomain(IEnumerable<TEntityViewModel> p)
        {
            return Mapper.Map<IEnumerable<TEntity>>(p);
        }
    }
}
