﻿using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Mappers
{
    public class PedidoMapper : MapperBase<Pedido, PedidoViewModel>, IPedidoMapper
    {
    }
}
