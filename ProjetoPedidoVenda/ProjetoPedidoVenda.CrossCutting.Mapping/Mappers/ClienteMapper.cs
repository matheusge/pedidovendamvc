﻿using ProjetoPedidoVenda.CrossCutting.Mapping.Interface;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping.Mappers
{
    public class ClienteMapper : MapperBase<Cliente, ClienteViewModel>, IClienteMapper
    {
    }
}
