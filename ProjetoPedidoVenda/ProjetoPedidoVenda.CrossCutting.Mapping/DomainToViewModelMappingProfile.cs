﻿using AutoMapper;
using ProjetoPedidoVenda.Domain.Entities;
using ProjetoPedidoVenda.Domain.ViewModel;

namespace ProjetoPedidoVenda.CrossCutting.Mapping
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<ClienteViewModel, Cliente>();
            CreateMap<ProdutoViewModel, Produto>();

            CreateMap<PedidoViewModel, Pedido>()
             .ForMember(x => x.PedidoItem, z => z.Ignore())
             .ForMember(x => x.Cliente, z => z.Ignore());

            CreateMap<PedidoItemViewModel, PedidoItem>()
                .ForMember(x => x.Pedido, z => z.Ignore())
                .ForMember(x => x.Produto, z => z.Ignore());
        }
    }
}
